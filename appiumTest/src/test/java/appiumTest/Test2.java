package appiumTest;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Test2 {
	AppiumDriver<MobileElement> dr1 ;
	@BeforeTest
	void setUp1() throws MalformedURLException {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("platformName", "android");
		cap.setCapability("deviceName", "Gionee");
		//		cap.setCapability("automationName", "UIautomator2");
		cap.setCapability("udid", "BEZH4TSS5TW85DDA");
		cap.setCapability("noReset", true);
		cap.setCapability("appPackage", "com.android.contacts");
		cap.setCapability("appActivity", "com.android.contacts.activities.DialtactsActivity");
		dr1 = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
	}
	@AfterTest
	void teardown1(){
		dr1.quit();
	}

	@Test
	void call_scenario5() {
		dr1.findElement(By.id("com.android.contacts:id/nine")).click();
		dr1.findElement(By.id("com.android.contacts:id/eight")).click();
		dr1.findElement(By.id("com.android.contacts:id/one")).click();
		dr1.findElement(By.id("com.android.contacts:id/nine")).click();
		dr1.findElement(By.id("com.android.contacts:id/two")).click();
		dr1.findElement(By.id("com.android.contacts:id/four")).click();
		dr1.findElement(By.id("com.android.contacts:id/nine")).click();
		dr1.findElement(By.id("com.android.contacts:id/five")).click();
		dr1.findElement(By.id("com.android.contacts:id/eight")).click();
		dr1.findElement(By.id("com.android.contacts:id/zero")).click();
		dr1.findElement(By.id("com.android.contacts:id/dialButton")).click();
		}
	
	
	}
