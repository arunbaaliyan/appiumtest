package appiumTest;

import static org.testng.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Test1 {

	AppiumDriver<MobileElement> dr ;
	@BeforeClass
	void setUp() throws MalformedURLException {
		System.out.println("Starting...");
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("platformName", "android");
		cap.setCapability("deviceName", "Gionee");
		cap.setCapability("udid", "BEZH4TSS5TW85DDA");
		cap.setCapability("appPackage", "com.android.calculator2");
		cap.setCapability("appActivity", "com.android.calculator2.Calculator");
		dr = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		System.out.println("Connection established...");
	}
	@AfterClass
	void teardown(){
		dr.quit();
	}

	@Test(priority = 1)
	void calc_scenario1() {
		
		dr.findElement(By.id("com.android.calculator2:id/digit9")).click();
		dr.findElement(By.id("com.android.calculator2:id/plus")).click();
		dr.findElement(By.id("com.android.calculator2:id/digit5")).click();
		dr.findElement(By.id("com.android.calculator2:id/equal")).click();
		String res = dr.findElement(By.xpath("//android.widget.EditText[@index = '1']")).getText();
		assertEquals(res, "=14");
//		System.out.println("9+5 "+res);
	}

	@Test(priority = 2)
	void calc_scenario2() {
		
		dr.findElement(By.id("com.android.calculator2:id/digit7")).click();
		dr.findElement(By.id("com.android.calculator2:id/mul")).click();
		dr.findElement(By.id("com.android.calculator2:id/digit6")).click();
		dr.findElement(By.id("com.android.calculator2:id/equal")).click();
		String res = dr.findElement(By.xpath("//android.widget.EditText[@index = '1']")).getText();
		assertEquals(res, "=42");
//		System.out.println("7*6 "+res);
	}
	
	@Test(priority = 3)
	void calc_scenario3() {
		
		dr.findElement(By.id("com.android.calculator2:id/digit9")).click();
		dr.findElement(By.id("com.android.calculator2:id/minus")).click();
		dr.findElement(By.id("com.android.calculator2:id/digit2")).click();
		dr.findElement(By.id("com.android.calculator2:id/equal")).click();
		String res = dr.findElement(By.xpath("//android.widget.EditText[@index = '1']")).getText();
		assertEquals(res, "=7");
//		System.out.println("9-2 "+res);
	}
	@Test(priority = 4)
	void calc_scenario4() {
		
		dr.findElement(By.id("com.android.calculator2:id/digit6")).click();
		dr.findElement(By.id("com.android.calculator2:id/div")).click();
		dr.findElement(By.id("com.android.calculator2:id/digit2")).click();
		dr.findElement(By.id("com.android.calculator2:id/equal")).click();
		String res = dr.findElement(By.xpath("//android.widget.EditText[@index = '1']")).getText();
		assertEquals(res, "=3");
//		System.out.println("6/2 "+res);
	}

}
