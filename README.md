# AppiumTest

SCENARIOS FOR APPIUM



TO find the appPackage and appActivity of an application using commandline, write this in commandline:

-> Run commandline

-> **adb shell**

-> (for android version  9.0 and below) ** dumpsys window windows | grep -E 'mCurrentFocus|mFocusedApp'**

-> (for android version 10.0 and above) **dumpsys window displays | grep -E 'mCurrentFocus|mFocusedApp'**